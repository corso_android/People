package com.example.people;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.example.people.database.ControllerDatabase;
import com.example.people.model.Person;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void testDatabase() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        ControllerDatabase controllerDatabase = ControllerDatabase.getInstance(appContext);
        controllerDatabase.deleteAllPeople();
        Assert.assertTrue("There are still some people", controllerDatabase.getPeople().size()==0);

        Person person1 = new Person("Mario", "Rossi", Calendar.getInstance());
        Person person2 = new Person("Mario", "Rossi", Calendar.getInstance());
        Person person3 = new Person("Mario", "Rossi", Calendar.getInstance());

        List<Person> people = new ArrayList<>();
        people.add(person1);
        people.add(person2);
        people.add(person3);

        for (Person person: people) {
            int pk = controllerDatabase.storePerson(person);
            person.setPk(pk);
        }

        Assert.assertTrue("Some people haven't been inserted", controllerDatabase.getPeople().size()==people.size());
    }
}

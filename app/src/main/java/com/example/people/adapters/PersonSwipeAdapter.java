package com.example.people.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.example.people.DatabasePerson;
import com.example.people.R;
import com.example.people.activities.InfoPersonActivity;
import com.example.people.model.Person;

import java.util.List;



public class PersonSwipeAdapter extends ArrayAdapter<Person> {

    private Activity activity;
    private List<Person> people;

    public PersonSwipeAdapter(Activity activity, int resource, List<Person> people) {
        super(activity, resource, people);
        this.activity = activity;
        this.people = people;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        final Person person = people.get(position);
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.listview_swipe, parent, false);

            holder = new ViewHolder(convertView);

            convertView.setTag(holder);

            holder.icon.setBackgroundDrawable(activity.getResources().getDrawable(R.mipmap.ic_launcher));
        } else {

            holder = (ViewHolder) convertView.getTag();
        }
        holder.textView1.setText(person.getSurname() + " " + person.getName());
        holder.textView2.setText(person.getBirthday().getTime().toString());
        holder.btnEdit.setOnClickListener(onEditListener(position, holder));
        holder.btnDelete.setOnClickListener(onDeleteListener(position, holder));
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, InfoPersonActivity.class);
                intent.putExtra("person", person);
                activity.startActivity(intent);
            }
        });

        return convertView;
    }

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    private View.OnClickListener onEditListener(final int position, final ViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Person person = people.get(position);
                Intent intent = new Intent(activity, InfoPersonActivity.class);
                intent.putExtra("person", person);
                activity.startActivity(intent);
            }
        };
    }


    private View.OnClickListener onDeleteListener(final int position, final ViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(activity)
                        .setTitle("Perdita dati")
                        .setMessage("Sei davvero sicuro di voler cancellare questa persona?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Person person = people.get(position);
                                dialog.dismiss();
                                people.remove(person);
                                DatabasePerson.getInstance().removePerson(person);
                                holder.swipeLayout.close();
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(R.drawable.delete)
                        .show();

            }
        };
    }



    private class ViewHolder {
        private ImageView icon;
        private TextView textView1;
        private TextView textView2;
        private Person person;
        private View btnDelete;
        private View btnEdit;
        private LinearLayout layout;
        private SwipeLayout swipeLayout;

        public ViewHolder(View v) {
            swipeLayout = (SwipeLayout) v.findViewById(R.id.swipe_layout);
            btnDelete = v.findViewById(R.id.delete);
            btnEdit = v.findViewById(R.id.edit_query);
            layout = (LinearLayout) v.findViewById(R.id.layout);
            icon = (ImageView) v.findViewById(R.id.imageView);
            textView1 = (TextView) v.findViewById(R.id.textView1);
            textView2 = (TextView) v.findViewById(R.id.textView2);
            swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        }
    }
}

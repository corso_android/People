package com.example.people.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.people.R;
import com.example.people.activities.InfoPersonActivity;
import com.example.people.model.Person;

import java.util.List;


public class PersonAdapter extends BaseAdapter {

    Context context;
    List<Person> people;
    LayoutInflater inflter;

    public PersonAdapter(Context context, List<Person> people) {
        this.context = context;
        this.people = people;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return people.size();
    }

    @Override
    public Object getItem(int i) {
        return people.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if(view==null) {
            view = inflter.inflate(R.layout.listview_adapter_person, null);
            LinearLayout row = (LinearLayout) view.findViewById(R.id.row);
            ImageView profile = (ImageView) view.findViewById(R.id.profile);
            TextView name = (TextView) view.findViewById(R.id.name);
            TextView date = (TextView) view.findViewById(R.id.date);
            viewHolder = new ViewHolder();
            viewHolder.profile = profile;
            viewHolder.name = name;
            viewHolder.date = date;
            viewHolder.row = row;
            viewHolder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, InfoPersonActivity.class);
                    intent.putExtra("person", viewHolder.person);
                    context.startActivity(intent);
                }
            });
            view.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        Person person = people.get(position);
        viewHolder.person = person;
        viewHolder.name.setText(viewHolder.person.getName()+" "+viewHolder.person.getSurname());
        viewHolder.date.setText(viewHolder.person.getBirthday().getTime().toString());
        return view;
    }

    public class ViewHolder {
        LinearLayout row;
        ImageView profile;
        TextView name;
        TextView date;
        Person person;
    }
}


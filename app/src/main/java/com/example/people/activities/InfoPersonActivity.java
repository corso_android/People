package com.example.people.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.people.DatabasePerson;
import com.example.people.model.Person;
import com.example.people.R;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;

public class InfoPersonActivity extends AppCompatActivity {

    @Bind(R.id.name)
    EditText name;
    @Bind(R.id.surname)
    EditText surname;

    Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_person);
        ButterKnife.bind(this);
        if(getIntent().hasExtra("person")){
            person = getIntent().getParcelableExtra("person");
            name.setText(person.getName());
            surname.setText(person.getSurname());
        }else{
            person = new Person();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Perdita modifiche")
                .setMessage("Stai per perdere le modifiche effetuate, sei sicuro?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        InfoPersonActivity.super.onBackPressed();


                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {
            new AlertDialog.Builder(this)
                    .setTitle("Perdita dati")
                    .setMessage("Sei davvero sicuro di voler cancellare questa persona?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            InfoPersonActivity.super.onBackPressed();


                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return true;
        }
        if (id == R.id.action_save) {
            if(checkForm()) {
                saveData();
                DatabasePerson.getInstance().addPerson(person);
                InfoPersonActivity.super.onBackPressed();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveData(){
        person.setName(name.getText().toString());
        person.setSurname(surname.getText().toString());
        person.setBirthday(Calendar.getInstance());
    }

    private boolean checkForm(){
        if(name.length()==0){
            name.requestFocus();
            name.setError("Inserisci il nome");
            return false;
        }
        if(surname.length()==0){
            surname.requestFocus();
            surname.setError("Inserisci il cognome");
            return false;
        }
        return true;
    }
}

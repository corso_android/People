package com.example.people;


import com.example.people.model.Person;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DatabasePerson {

    private static DatabasePerson databasePerson;
    private static List<Person> people;

    private DatabasePerson(){

    }

    public static DatabasePerson getInstance(){
        if(databasePerson==null){
            databasePerson = new DatabasePerson();
            people = new ArrayList<>();
            people.add(new Person("Mario","Rossi", Calendar.getInstance()));
            people.add(new Person("Mario","Verdi", Calendar.getInstance()));
            people.add(new Person("Giuseppe","Garibaldi", Calendar.getInstance()));
        }
        return databasePerson;
    }

    public void addPerson(Person person){
        people.add(person);
    }
    public void removePerson(Person person){
        people.remove(person);
    }

    public static List<Person> getPeople() {
        return people;
    }

    public static void setPeople(List<Person> people) {
        DatabasePerson.people = people;
    }
}

package com.example.people.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import com.example.people.database.cursors.PersonCursor;
import com.example.people.model.Person;

import java.util.ArrayList;
import java.util.List;


public class DatabaseManager {

    private Integer mOpenCounter = 0;
    private static DatabaseManager instance;
    private static DBHelper mDatabaseHelper;
    private SQLiteDatabase db;
    private SQLiteDatabase mDatabase;

    private DatabaseManager(Context context) {
        mDatabaseHelper = new DBHelper(context);
        db = openDatabase();
    }

    public static synchronized DatabaseManager getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseManager(context);
        }
        return instance;
    }

    private synchronized SQLiteDatabase openDatabase() {
        mOpenCounter += 1;
        if (mOpenCounter == 1) {
            mDatabase = mDatabaseHelper.getWritableDatabase();
        }
        return mDatabase;
    }

    int storeObject(String table, ContentValues values) {
        int pk;
        try {

            pk = (int) db.insertOrThrow(table, null, values);
        } catch (SQLiteConstraintException e) {
            pk = values.getAsInteger("pk");
            db.update(table, values, "pk=" + pk, null);
            return values.getAsInteger("pk");
        }
        return pk;
    }

    void deleteObjectByPk(String table, int pk) {
        db.delete(table, "pk = " + pk, null);
    }

    void deleteAllItemsFromTable(String table) {

        db.execSQL("DELETE FROM " + table);

    }

    List<Person> getAllPeople() {
        List<Person> people = new ArrayList<>();
        Cursor cursor = db.query("PERSON",
                null, null, null, null, null, null, null);

        cursor.moveToFirst();
        PersonCursor personCursor = new PersonCursor();
        if (cursor.getCount() != 0) {
            while (!cursor.isAfterLast()) {
                Person person = personCursor.retrieve(cursor);
                people.add(person);
                cursor.moveToNext();
            }

        }
        cursor.close();
        return people;
    }

}

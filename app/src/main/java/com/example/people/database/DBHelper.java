package com.example.people.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "PERSON.db";
    private static final int DATABASE_VERSION = 1;

    public static String TABLE_PERSON =  "CREATE TABLE PERSON (pk INTEGER PRIMARY KEY autoincrement, name TEXT, surname TEXT, birthday INTEGER)";

    private Context context;

    DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_PERSON);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}

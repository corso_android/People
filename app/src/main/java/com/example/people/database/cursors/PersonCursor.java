package com.example.people.database.cursors;


import android.database.Cursor;

import com.example.people.model.Person;

import java.util.Calendar;

public class PersonCursor {


    public Person retrieve(Cursor cursor){
        Integer pk = cursor.getInt(cursor.getColumnIndex("pk"));
        String name = cursor.getString(cursor.getColumnIndex("name"));
        String surname = cursor.getString(cursor.getColumnIndex("surname"));
        Calendar birtdhay = Calendar.getInstance();
        Long timestamp = cursor.getLong(cursor.getColumnIndex("birthday"));
        birtdhay.setTimeInMillis(timestamp*1000);
        return new Person(pk, name, surname, birtdhay);
    }

}

package com.example.people.database.contentValues;


import android.content.ContentValues;

import com.example.people.model.Person;

public class PersonContentValues {


    public ContentValues getContentValues(Person person){
        ContentValues contentValues = new ContentValues();
        contentValues.put("pk",person.getPk());
        contentValues.put("name", person.getName());
        contentValues.put("surname", person.getSurname());
        contentValues.put("birthday", person.getBirthday().getTimeInMillis() / 1000);
        return contentValues;
    }

}

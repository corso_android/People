package com.example.people.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

public class Person implements Parcelable {

    private Integer pk;
    private String name;
    private String surname;
    private Calendar birthday;

    public Person(Integer pk, String name, String surname, Calendar birthday) {
        this.pk = pk;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    public Person(String name, String surname, Calendar birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    public Person() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Calendar getBirthday() {
        return birthday;
    }

    public void setBirthday(Calendar birthday) {
        this.birthday = birthday;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.pk);
        dest.writeString(this.name);
        dest.writeString(this.surname);
        dest.writeSerializable(this.birthday);
    }

    protected Person(Parcel in) {
        this.pk = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.surname = in.readString();
        this.birthday = (Calendar) in.readSerializable();
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel source) {
            return new Person(source);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
}

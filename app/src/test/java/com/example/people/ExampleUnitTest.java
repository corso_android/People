package com.example.people;

import com.example.people.model.Person;

import org.junit.Assert;
import org.junit.Test;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
       DatabasePerson databasePerson = DatabasePerson.getInstance();
        Assert.assertTrue("Il metodo non funziona", databasePerson.getPeople().size()==3);
        databasePerson.addPerson(new Person());
        Assert.assertTrue("Il metodo di inserimento non funziona correttamente", databasePerson.getPeople().size()==4);
    }
}